export const state = {}

export const mutations = {}

export const getters = {}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    // connect to Bitshares
    dispatch('account/checkCachedUserData', null, { root: true })
    dispatch('connection/initConnection', null, { root: true })
    // todo: get cached user data
  },
  initUserData: async ({ state, rootGetters, dispatch }) => {
    const userId = rootGetters['account/getAccountUserId']

    await Promise.all([
      dispatch('assets/fetchDefaultAssets', null, { root: true }),
      dispatch('account/fetchCurrentUser', null, { root: true }),
      dispatch('transactions/fetchComissions', null, { root: true }),
      dispatch(
        'operations/fetchAndSubscribe',
        { userId, limit: 50 },
        { root: true }
      ),
    ])
    const balances = { ...rootGetters['account/getCurrentUserBalances'] }
    const defaultAssetsIds = rootGetters['assets/getDefaultAssetsIds']
    defaultAssetsIds.forEach(id => {
      if (balances[id]) return
      balances[id] = { balance: 0 }
    })
    await dispatch(
      'assets/fetchAssets',
      {
        assets: Object.keys(balances),
      },
      { root: true }
    )
  },
  unsubFromUserData({ dispatch }) {
    dispatch('operations/unsubscribeFromUserOperations', null, { root: true })
  },
  resetUserData({ dispatch }) {
    dispatch('operations/unsubscribeFromUserOperations', null, { root: true })
    dispatch('account/clearCurrentUserData', null, { root: true })
    dispatch('operations/resetState', null, { root: true })
  },
}
