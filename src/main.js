import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './app'
import router from '@router'
import store from '@state/store'

import 'vuetify/dist/vuetify.min.css'

// Helpers
import colors from 'vuetify/es5/util/colors'

// Don't warn about using the dev version of Vue in development
Vue.config.productionTip = process.env.NODE_ENV === 'production'

// Vue.use(Vuetify);
Vue.use(Vuetify, {
  theme: {
    primary: '#1867c0',
    secondary: colors.blue.lighten4, // #FFCDD2
    accent: colors.indigo.base, // #3F51B5
    error: '#b71c1c',
  },
})

Vue.prototype.$vuetify.theme.error = '#b71c1c'

const app = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#mount')

// If running inside Cypress
if (window.Cypress) {
  // Attach the app to the window, which can be useful
  // for manually setting state in Cypress commands
  // such as `cy.logIn()`
  window.__app__ = app
}

window.crypto.randomBytes = require('randombytes')
